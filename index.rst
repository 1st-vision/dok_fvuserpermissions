.. |label| replace:: USER-Berechtigung für Konzerne
.. |snippet| replace:: FvUserPermissions
.. |Author| replace:: 1st Vision GmbH
.. |Entwickler| replace:: Internetfabrik
.. |minVersion| replace:: 5.3.4
.. |maxVersion| replace:: 5.3.4
.. |Version| replace:: 1.0.8
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Mit diesem Plugin haben Sie die Möglichkeit das Anlegen und Verwalten von Kundenkonten, mit verschiedenen Rechten, für Mitarbeiter verschiedener Unternehmen in Ihrem Shop zu gestatten.

Frontend
--------
Im Shop werden die Mitarbeiter getrennt von den Shopware-Konten abgelegt, somit wird eine saubere Datenstruktur erschaffen und es gibt die eMail-Adresse der Mitarbeiter nur einmal. Wenn die eMail-Adresse als Ansprechpartner bei mehreren OfficeLine Adressen hinterlegt ist werden Sie entsprechend auf die Shopware-Konten zugeordnet und im Shop wird beim Einloggen nach dem entsprechenden Konto gefragt. Man hat auch im eingeloggten Zustand jederzeit die Möglichkeit den Account zu wechseln. Beim Wechsel des Kontos wird auch gefragt ob man den Warenkorb ersetzen möchte oder dazu packen möchte oder wechseln möchte.
Die Warenkörbe werden für jeden „fv_user“ persistent gespeichert. Mitarbeiter mit dem Recht „Mitarbeiter verwalten“ oder Admins können für andere Mitarbeiter, desselben Konzerns, einkaufen. 

Hierbei haben sie drei Möglichkeiten:
 
:Meinen Warenkorb übertragen: „Ihr Warenkorb“ löscht den Warenkorb des anderen Mitarbeiters und fügt stattdessen ihre Produkte hinzu.
:Den Warenkorb vom „Mitarbeiter“ übernehmen: „Ihr Warenkorb“ wird gelöscht und durch den des Mitarbeiters ersetzt. 
:Meinen Warenkorb mit dem von „Mitarbeiter“ zusammenführen: Die Artikel aus „Ihrem Warenkorb“ werden dem Warenkorb des Mitarbeiters hinzugefügt. 

.. image:: FvUserPermissions2.png

Sollte der Mitarbeiter, zu dem gewechselt werden soll, nicht über das Recht verfügen Produkte zu kaufen wird eine entsprechende Meldung ausgegeben und „Ihr Warenkorb“ kann nicht mit übernommen oder zusammengeführt werden.

.. image:: FvUserPermissions3.png

In jedem Fall ist nach dem Wechsel „Ihr Warenkorb“ leer.

Bei dem Bestellabschluss wird nochmal darauf hingewiesen mit welchem Shop-konto man die Bestellung tätigt.
Im Shop wird unter Mein Konto ein neuer Reiter angezeigt namens „Mitarbeiter“.

.. image:: FvUserPermissions4.png

Hier kann der bereits erstellt Mitarbeiter bearbeitet werden:

.. image:: FvUserPermissions5.png

In dieser Ansicht können die Persönlichen Daten geändert werden und die Berechtigung gesetzt oder entzogen werden.
Auch das Passwort für den Kunden kann neu gesetzt werden.

Es kann ein neue Mitarbeiter erstellt werden:

.. image:: FvUserPermissions6.png

Oder auch ein Mitarbeiter gelöscht werden.

.. image:: FvUserPermissions7.png

In der Übersicht der Mitarbeiter kann man auch eine Berechtigungsübersicht einblenden lassen:

.. image:: FvUserPermissions8.png

Hier werden die Berechtigungen angezeigt welche der Mitarbeiter hinterlegt hat.

Zusätzlich kann der Administrator auch die Identität des Mitarbeiters annehmen und die Berechtigungen zu überprüfen.

.. image:: FvUserPermissions9.png

Folgende Berechtigung sind im Standard enthalten:

:Mitarbeiter verwalten:
:Lieferadresse anlegen:
:Lieferadresse bearbeiten:
:Lieferadresse löschen:
:Standardlieferadresse festlegen:
:Preise sehen:
:Passwort ändern:
:Artikel kaufen:
:Bestellhistorie einsehen:
:Email ändern:
:Berechtigungen vergeben (hat nur der Admin, kann nicht vergeben werden):

Ein Mitarbeiter der das Recht „Mitarbeiter verwalten“ hat darf Mitarbeiter mit den Standard-Rechten anlegen und bearbeiten. Er darf diese Rechte allerdings weder ändern noch neue vergeben. Er kann auch für andere Mitarbeiter einkaufen, deren Warenkorb übernehmen oder seinen mit ihrem zusammenlegen. Aber: Er ist kein Admin. 


Backend
-------
In der OfficeLine werden bei den Adressen/Kontokorrents die Shop-Konten definiert und die Ansprechpartner werden im Shop zu Mitarbeitern.

Optional gibt es eine Aufgaben-Center Auswertung um dies besser zu steuern.
___________________________________________________________________________

.. image:: FvUserPermissions1.png

Mit Hilfe dieser AC-Auswertung können Sie alle Adressen mit Kontokorrents anzeigen lassen und per Klicke als Shop-Kunde und mit/ohne Konzern-Login freischalten. Im Register Ansprechpartner können Sie die Ansprechpartner bearbeiten und als Admin deklarieren und den Ansprechpartner als Mitarbeiter im Shop auch deaktivieren.

Der Wechsel zu den einzelnen Mitarbeitern ist auch über das Shop-Backend, im Bereich Kunden, möglich.

.. image:: FvUserPermissions10.png

Anmerkungen: 
____________

:E-Mail-Adressen müssen über alle Subshops einzigartig sein. Die Scope-Funktion von Shopware kann nicht verwendet werden.:
:Kunden die nicht eingeloggt sind sehen immer die Preise für Endkunden und können Einkaufen.:
:Eingeloggte Kunden werden nicht automatisch, nach einer gewissen Zeit, ausgeloggt.:



technische Beschreibung
------------------------
OfficeLine:
___________

:[KHKAdressen].[USER_Webshopaktiv] boolean: definiert ob der Kunde ein Shop Kunde werden soll
:[KHKKontokorrent].[USER_konzernlogin] boolean: definiert ob der Kunde ein Konzern-Login erhalten soll
:[KHKAnsprechpartner].[USER_shopdeaktiv] boolean: definiert ob der Ansprechpartner in den Shop hochgeladen werden soll


Shop-Datenbank:
_______________
:s_user: Hier werden die übergreifenden Accounts der Unternehmen oder die Accounts von Privat-Kunden angelegt. Die Rechnungsadresse wird immer aus dieser Tabelle bezogen.

:fv_user: Enthält die einzelnen Accounts der Mitarbeiter.

:fv_user_s_user: Ordnet die Mitarbeiter aus fv_user den Firmen-Accounts in s_user zu. Enthält außerdem die Information ob ein Mitarbeiter in diesem Unternehmen Admin-Rechte hat.

:fv_user_s_user_permission: Ordnet den Mitarbeiten die jeweiligen Rechte für ein Unternehmen zu.

:fv_permissions: Enthält die einzelnen Rechte die pro Shop vergeben werden. Die Spalte „description“ dient nur der Verständlichkeit, wird aber nicht ausgelesen. Die Texte die im Frontend angezeigt werden, werden über die Textbausteine von Shopware verwaltet. Die Spalte „administrable“ sagt aus ob diese Permission einem Mitarbeiter durch einen Admin zugewiesen werden kann. Die Spalte „default“ zeigt an ob ein neuer Mitarbeiter dieses Recht standardmäßig hat.

Bei einer Bestellung werden folgende Werte weggeschrieben:
__________________________________________________________
In die Tabelle s_order_attributes

:fv_user_order_description: (Bestellung getätigt von fv_user-ID 38 (Enrico Nieger)) Wer wirkliche die Bestellung getätigt hat, hier erkennt man falls der Admin für einen Mitarbeiter die Bestellung getätigt hat.

:fv_user_order_user_email: die Email-Adresse des Mitarbeiters
:fv_user_order_user_salutation: die Anrede des Mitarbeiters
:fv_user_order_user_firstname: Den Vorname des Mitarbeiters
:fv_user_order_user_lastname: den Nachname des Mitarbeiters
:fv_user_order_price_show: wenn der Mitarbeiter das Recht „Preise sehen“ hat wird der Wert auf 1 gesetzt ansonsten 0


.. image:: FvUserPermissions11.png

Das Zusammenspiel mit dem Plugin "Vorgesetzten-Regelung für Bestellungen" wurde integriert.

Modifizierte Template-Dateien
-----------------------------
sämtlichen tpl-Dateien


